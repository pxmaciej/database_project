-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 21 Kwi 2020, 23:25
-- Wersja serwera: 8.0.19
-- Wersja PHP: 7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `biblioteka`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `autor`
--

CREATE TABLE `autor` (
  `id_autora` int NOT NULL,
  `imie` varchar(20) DEFAULT NULL,
  `nazwisko` varchar(20) NOT NULL,
  `drugie_imie` varchar(20) DEFAULT NULL,
  `pseudonim` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `noblista` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `autor`
--

INSERT INTO `autor` (`id_autora`, `imie`, `nazwisko`, `drugie_imie`, `pseudonim`, `email`, `noblista`) VALUES
(1, 'Lukasz', 'Najder', '', 'luke', 'lukaszn@o2.pl', 'NIE'),
(2, 'Alex', 'Marwood', '', '', 'alex@gmail.com', 'NIE'),
(3, 'Cassandra', 'Clare', '', 'Casie', 'cass@wp.pl', 'NIE'),
(4, 'Wasley', 'Chu', '', '', 'WChu@tut.com', 'NIE'),
(6, 'Andrzej', 'Sapkowski', '', 'SAPEK', 'sap@gg.com', 'NIE');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `autor_ksiazka`
--

CREATE TABLE `autor_ksiazka` (
  `id_autora` int DEFAULT NULL,
  `id_ksiazki` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Zrzut danych tabeli `autor_ksiazka`
--

INSERT INTO `autor_ksiazka` (`id_autora`, `id_ksiazki`) VALUES
(1, 5),
(2, 6),
(3, 8),
(4, 8),
(6, 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane_admina`
--

CREATE TABLE `dane_admina` (
  `id` int NOT NULL,
  `nazwa_admina` varchar(50) NOT NULL,
  `haslo_admina` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Zrzut danych tabeli `dane_admina`
--

INSERT INTO `dane_admina` (`id`, `nazwa_admina`, `haslo_admina`) VALUES
(1, 'star', 'vertigo');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id_klienta` int NOT NULL,
  `PESEL` char(11) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `miasto` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `telefon` int DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `nazwa_uzytkownika` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `haslo` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`id_klienta`, `PESEL`, `miasto`, `telefon`, `email`, `nazwa_uzytkownika`, `haslo`) VALUES
(1, '77777665432', 'Legnica', 7844356, 'haw', 'hawke', '1234');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ksiazka`
--

CREATE TABLE `ksiazka` (
  `id_ksiazki` int NOT NULL,
  `tytul` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `podtytul` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `gatunek` varchar(20) CHARACTER SET latin1 COLLATE latin1_danish_ci DEFAULT NULL,
  `oprawa` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `ISBN` char(13) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `naklad` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `ksiazka`
--

INSERT INTO `ksiazka` (`id_ksiazki`, `tytul`, `podtytul`, `gatunek`, `oprawa`, `ISBN`, `naklad`) VALUES
(5, 'Moja osoba', NULL, 'powiesc', 'miekka', '9788380499829', 24),
(6, 'Zatroty Ogrod', NULL, 'powiesc', 'twarda', '9788381256186', 44),
(8, 'Magia', NULL, 'fantastyka', 'miekka', '9788366517905', 20),
(9, 'Wiedzmin', NULL, 'fantasy', 'miekka', '9788375780642', 32);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wydawnictwo`
--

CREATE TABLE `wydawnictwo` (
  `id_wydawnictwa` int NOT NULL,
  `nazwa` varchar(35) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `data_wydania` date DEFAULT NULL,
  `miejsce_wydania` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `typ_publikacji` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `liczba_stron_wydania` int DEFAULT NULL,
  `id_ksiazki` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `wydawnictwo`
--

INSERT INTO `wydawnictwo` (`id_wydawnictwa`, `nazwa`, `data_wydania`, `miejsce_wydania`, `typ_publikacji`, `liczba_stron_wydania`, `id_ksiazki`) VALUES
(1, 'Albatros', '2000-01-05', 'Warszawa', 'ksiazka', 224, 5),
(2, 'Albatros', '2011-12-28', 'Warszawa', 'ksiazka', 384, 6),
(4, 'We need YA', '2012-03-14', 'Katowice', 'ksiazka', 322, 8),
(5, 'SuperNowa', '2008-01-09', 'Warszawa', 'ksiazka', 432, 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wypozyczenia`
--

CREATE TABLE `wypozyczenia` (
  `id_wypozyczenia` int NOT NULL,
  `data_wypozyczenia` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `klient` int DEFAULT NULL,
  `id_ksiazki` int DEFAULT NULL,
  `data_oddania` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `wypozyczenia`
--

INSERT INTO `wypozyczenia` (`id_wypozyczenia`, `data_wypozyczenia`, `klient`, `id_ksiazki`, `data_oddania`) VALUES
(1, '2020-04-19 21:17:37', 1, 8, '2020-05-19 21:17:29');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`id_autora`);

--
-- Indeksy dla tabeli `autor_ksiazka`
--
ALTER TABLE `autor_ksiazka`
  ADD KEY `id_autora` (`id_autora`),
  ADD KEY `id_ksiazki` (`id_ksiazki`);

--
-- Indeksy dla tabeli `dane_admina`
--
ALTER TABLE `dane_admina`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nazwa_admina` (`nazwa_admina`);

--
-- Indeksy dla tabeli `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`id_klienta`),
  ADD UNIQUE KEY `Nazwa_uzytkownika` (`nazwa_uzytkownika`),
  ADD UNIQUE KEY `PESEL` (`PESEL`);

--
-- Indeksy dla tabeli `ksiazka`
--
ALTER TABLE `ksiazka`
  ADD PRIMARY KEY (`id_ksiazki`),
  ADD UNIQUE KEY `ISBN` (`ISBN`),
  ADD UNIQUE KEY `ISBN_2` (`ISBN`);

--
-- Indeksy dla tabeli `wydawnictwo`
--
ALTER TABLE `wydawnictwo`
  ADD PRIMARY KEY (`id_wydawnictwa`),
  ADD KEY `id_ksiazki` (`id_ksiazki`);

--
-- Indeksy dla tabeli `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  ADD PRIMARY KEY (`id_wypozyczenia`),
  ADD KEY `klient` (`klient`),
  ADD KEY `id_ksiazki` (`id_ksiazki`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `autor`
--
ALTER TABLE `autor`
  MODIFY `id_autora` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `dane_admina`
--
ALTER TABLE `dane_admina`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `klient`
--
ALTER TABLE `klient`
  MODIFY `id_klienta` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ksiazka`
--
ALTER TABLE `ksiazka`
  MODIFY `id_ksiazki` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `wydawnictwo`
--
ALTER TABLE `wydawnictwo`
  MODIFY `id_wydawnictwa` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  MODIFY `id_wypozyczenia` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `autor_ksiazka`
--
ALTER TABLE `autor_ksiazka`
  ADD CONSTRAINT `Autor_Ksiazka_ibfk_1` FOREIGN KEY (`id_autora`) REFERENCES `autor` (`id_autora`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Autor_Ksiazka_ibfk_2` FOREIGN KEY (`id_ksiazki`) REFERENCES `ksiazka` (`id_ksiazki`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wydawnictwo`
--
ALTER TABLE `wydawnictwo`
  ADD CONSTRAINT `Wydawnictwo_ibfk_1` FOREIGN KEY (`id_ksiazki`) REFERENCES `ksiazka` (`id_ksiazki`);

--
-- Ograniczenia dla tabeli `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  ADD CONSTRAINT `Wypozyczenia_ibfk_1` FOREIGN KEY (`klient`) REFERENCES `klient` (`id_klienta`),
  ADD CONSTRAINT `Wypozyczenia_ibfk_2` FOREIGN KEY (`id_ksiazki`) REFERENCES `autor_ksiazka` (`id_ksiazki`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
