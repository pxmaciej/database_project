#ifndef ZARZADZAJ_UZYTKOWNIKAMI_H
#define ZARZADZAJ_UZYTKOWNIKAMI_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>
#include <QSqlTableModel>

namespace Ui {
class zarzadzaj_uzytkownikami;
}

class zarzadzaj_uzytkownikami : public QDialog
{
    Q_OBJECT

public:
    explicit zarzadzaj_uzytkownikami(QWidget *parent = nullptr);
    ~zarzadzaj_uzytkownikami();

private:
    Ui::zarzadzaj_uzytkownikami *ui;
    QSqlTableModel *tableModel;
};

#endif // ZARZADZAJ_UZYTKOWNIKAMI_H
