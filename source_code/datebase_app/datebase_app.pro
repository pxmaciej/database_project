#-------------------------------------------------
#
# Project created by QtCreator 2020-03-10T18:43:11
#
#-------------------------------------------------

QT       += core gui sql
CONFIG += c++11
QT += core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = datebase_app
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    k_zarzadzaj_wypozyczeniami.cpp \
    k_znajdz_ksiazke.cpp \
        main.cpp \
        mainwindow.cpp \
    usersign.cpp \
    klientpan.cpp \
    adminsign.cpp \
    adminpan.cpp \
    dodaj_ksiazke.cpp \
    dodaj_autora.cpp \
    przydziel_autora.cpp \
    zarzadzaj_uzytkownikami.cpp \
    zasoby_biblioteki.cpp \
    dodaj_wydawnictwo.cpp \
    wyswietl_uzytkownikow.cpp \
    znajdz_ksiazke.cpp \
    zarzadzaj_wypozyczeniami.cpp

HEADERS += \
    k_zarzadzaj_wypozyczeniami.h \
    k_znajdz_ksiazke.h \
        mainwindow.h \
    usersign.h \
    klientpan.h \
    adminsign.h \
    adminpan.h \
    dodaj_ksiazke.h \
    dodaj_autora.h \
    przydziel_autora.h \
    zarzadzaj_uzytkownikami.h \
    zasoby_biblioteki.h \
    dodaj_wydawnictwo.h \
    wyswietl_uzytkownikow.h \
    znajdz_ksiazke.h \
    zarzadzaj_wypozyczeniami.h

FORMS += \
    k_zarzadzaj_wypozyczeniami.ui \
    k_znajdz_ksiazke.ui \
        mainwindow.ui \
    usersign.ui \
    klientpan.ui \
    adminsign.ui \
    adminpan.ui \
    dodaj_ksiazke.ui \
    dodaj_autora.ui \
    przydziel_autora.ui \
    zarzadzaj_uzytkownikami.ui \
    zasoby_biblioteki.ui \
    dodaj_wydawnictwo.ui \
    wyswietl_uzytkownikow.ui \
    znajdz_ksiazke.ui \
    zarzadzaj_wypozyczeniami.ui

RESOURCES += \
    items.qrc \
    items.qrc \
    items.qrc \
    items.qrc
