#ifndef ADMINPAN_H
#define ADMINPAN_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>
#include "dodaj_ksiazke.h"
#include "dodaj_autora.h"
#include "przydziel_autora.h"
#include "zasoby_biblioteki.h"
#include "dodaj_wydawnictwo.h"
#include "wyswietl_uzytkownikow.h"
#include "znajdz_ksiazke.h"
#include "zarzadzaj_wypozyczeniami.h"
#include "zarzadzaj_uzytkownikami.h"

namespace Ui {
class adminpan;
}

class adminpan : public QDialog
{
    Q_OBJECT

public:
    explicit adminpan(QWidget *parent = 0);
    ~adminpan();

private slots:
    void on_dodaj_ksiazke_clicked();

    void on_dodaj_autora_clicked();

    void on_przydziel_autora_clicked();

    void on_zasoby_biblioteki_clicked();

    void on_dodaj_wydawnictwo_clicked();

    void on_pushButton_clicked();

    void on_uzytkownicy_systemu_clicked();

    void on_znajdz_ksiazke_clicked();

    void on_zarzadzaj_wypozyczeniami_clicked();

    void on_zarzadzaj_uzytkownikami_clicked();

private:
    Ui::adminpan *ui;
    dodaj_ksiazke *dodajksiazke;
    dodaj_autora  *dodajautora;
    przydziel_autora *przydzielautora;
    zasoby_biblioteki *zasobybiblioteki;
    dodaj_wydawnictwo *dodajwydawnictwo;
    wyswietl_uzytkownikow *wyswietluzytkownikow;
    znajdz_ksiazke *znajdzksiazke;
    zarzadzaj_wypozyczeniami *zarzadzajwypozyczeniami;
    zarzadzaj_uzytkownikami *zarzadzajuzytkownikami;

};

#endif // ADMINPAN_H
