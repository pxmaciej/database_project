#include "wyswietl_uzytkownikow.h"
#include "ui_wyswietl_uzytkownikow.h"

wyswietl_uzytkownikow::wyswietl_uzytkownikow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::wyswietl_uzytkownikow)
{
    ui->setupUi(this);

    querymodel=new QSqlQueryModel();
    querymodel->setQuery("SELECT * FROM klient");

    ui->tableView->setModel(querymodel);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

}

wyswietl_uzytkownikow::~wyswietl_uzytkownikow()
{
    delete ui;
}
