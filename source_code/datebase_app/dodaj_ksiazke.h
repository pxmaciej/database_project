#ifndef DODAJ_KSIAZKE_H
#define DODAJ_KSIAZKE_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class dodaj_ksiazke;
}

class dodaj_ksiazke : public QDialog
{
    Q_OBJECT

public:
    explicit dodaj_ksiazke(QWidget *parent = nullptr);
    ~dodaj_ksiazke();

private slots:
    void on_pushButton_clicked();

private:
    Ui::dodaj_ksiazke *ui;
};

#endif // DODAJ_KSIAZKE_H
