#include "zarzadzaj_wypozyczeniami.h"
#include "ui_zarzadzaj_wypozyczeniami.h"
#include <QSqlQueryModel>

zarzadzaj_wypozyczeniami::zarzadzaj_wypozyczeniami(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::zarzadzaj_wypozyczeniami)
{
    ui->setupUi(this);

    tableModel= new QSqlRelationalTableModel(this);
    delegate = new QSqlRelationalDelegate(this);

    tableModel->setTable("wypozyczenia");
    tableModel->setRelation(2, QSqlRelation("klient","id_klienta","nazwa_uzytkownika"));
    tableModel->setRelation(3, QSqlRelation("ksiazka","id_ksiazki","tytul"));

    tableModel->select();

    ui->tableView->setModel(tableModel);
    ui->tableView->setItemDelegate(delegate);

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

zarzadzaj_wypozyczeniami::~zarzadzaj_wypozyczeniami()
{
    delete ui;
}
