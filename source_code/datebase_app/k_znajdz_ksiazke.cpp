#include "k_znajdz_ksiazke.h"
#include "ui_k_znajdz_ksiazke.h"
#include <QDebug>




k_znajdz_ksiazke::k_znajdz_ksiazke(QWidget *parent) :
    QDialog(parent),

    ui(new Ui::k_znajdz_ksiazke)
{
    ui->setupUi(this);
}
 int kid;

void k_znajdz_ksiazke::on_szukaj_clicked()
{

//************************************************szukaj ksiazki*****************************************************************************************
       QString tytul=ui->linetytul->text();
       QString podtytul=ui->linepodtytul->text();
       QString gatunek=ui->linegatunek->text();
       QString oprawa=ui->lineoprawa->text();
       QString ISBN=ui->lineisbn->text();
       QString imie=ui->lineautorimie->text();
       QString nazwisko=ui->lineautornazwisko->text();


       QString query = "SELECT autor.imie AS imię, autor.nazwisko, ksiazka.tytul AS tytuł, ksiazka.podtytul AS podtytuł, ksiazka.gatunek, ksiazka.oprawa, ksiazka.ISBN ,ksiazka.id_ksiazki, ksiazka.naklad FROM(( autor INNER JOIN autor_ksiazka ON autor.id_autora=autor_ksiazka.id_autora) INNER JOIN ksiazka ON autor_ksiazka.id_ksiazki=ksiazka.id_ksiazki) WHERE tytul='"+ tytul + "' OR gatunek='" +gatunek +"' OR podtytul ='"+podtytul +"' OR oprawa ='"+oprawa +"' OR autor.imie = '"+imie +"' OR autor.nazwisko = '"+nazwisko +"' OR ISBN='" +ISBN +"'";


       model->setQuery(query);

       QTableView *view = new QTableView;
       view->setModel(model);

       ui->tableView->setModel(model);
        ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//*****************************************************************************************************************************************************
}

void k_znajdz_ksiazke::on_wypozycz_clicked()
{
//************************************************wypozycz**********spedziłem tu kurwa 2 dnia zycia*****************************************************
    QSqlQuery qry2;

    QString idksiazkis = QString::number(idksiazki);

    if(naklad>0){
        qry2.prepare("INSERT INTO wypozyczenia(klient,id_ksiazki,data_oddania)"
                    "VALUES (:klient, :id_ksiazki, :data_oddania)");

        qry2.bindValue(":klient", kid);
        qry2.bindValue(":id_ksiazki", idksiazki);
        qry2.bindValue(":data_oddania", curent_time.addMonths(1));

        if(qry2.exec()){
            QMessageBox::information(this, "Wypożyczenie", "Wypożyczono książke");
            int minus_naklad = (naklad -1);

            qry2.prepare("UPDATE ksiazka SET naklad =:n_aklad WHERE id_ksiazki='"+ idksiazkis + "'");
            qry2.bindValue(":n_aklad", minus_naklad);
            qry2.exec();

        }else{
            QMessageBox::warning(this, "Wypożyczenie", "Nie udało się wypożyczyć");
        }
}else{
        QString nakladcos = QString::number(naklad);
        QMessageBox::warning(this, "Wypożyczenie", "Nie udało się wypożyczyć, obecny nakład to :" ,nakladcos);
    }



/*XDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDto nie jest kurwa zartXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXDXD*/
}

void k_znajdz_ksiazke::on_tableView_activated(const QModelIndex &index)
{

//************************************************kliknij na wynik z tabeli i przypisz do pojemników line**********************************************
    ui->linetytul->setText(model->query().value(2).toString());
    ui->linepodtytul->setText(model->query().value(3).toString());
    ui->linegatunek->setText(model->query().value(4).toString());
    ui->lineoprawa->setText(model->query().value(5).toString());
    ui->lineisbn->setText(model->query().value(6).toString());
    ui->lineautorimie->setText(model->query().value(0).toString());
    ui->lineautornazwisko->setText(model->query().value(1).toString());
    idksiazki = model->query().value(7).toInt();
    naklad = model->query().value(8).toInt();
//*****************************************************************************************************************************************************
}

void k_znajdz_ksiazke::klientid (int klient)
{
   kid=klient;

}


void k_znajdz_ksiazke::on_pushButton_clicked()
{
    this->close();
    QWidget *parent = this->parentWidget();
    parent->show();
}

k_znajdz_ksiazke::~k_znajdz_ksiazke()
{
    delete ui;
}

void k_znajdz_ksiazke::on_szukaj_2_clicked()
{
    querymodel=new QSqlQueryModel();
    querymodel->setQuery("SELECT autor.imie AS imię, autor.nazwisko, ksiazka.tytul AS tytuł, ksiazka.podtytul AS podtytuł, ksiazka.id_ksiazki FROM autor INNER JOIN autor_ksiazka ON autor_ksiazka.id_autora=autor.id_autora INNER JOIN ksiazka ON autor_ksiazka.id_ksiazki=ksiazka.id_ksiazki");

    ui->tableView->setModel(querymodel);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}
