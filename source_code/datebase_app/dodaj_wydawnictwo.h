#ifndef DODAJ_WYDAWNICTWO_H
#define DODAJ_WYDAWNICTWO_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class dodaj_wydawnictwo;
}

class dodaj_wydawnictwo : public QDialog
{
    Q_OBJECT

public:
    explicit dodaj_wydawnictwo(QWidget *parent = 0);
    ~dodaj_wydawnictwo();

private slots:
    void on_Dodaj_clicked();

    void on_pobierz_liste_clicked();

private:
    Ui::dodaj_wydawnictwo *ui;
};

#endif // DODAJ_WYDAWNICTWO_H
