#ifndef USERSIGN_H
#define USERSIGN_H

#include <QDialog>
#include <QMessageBox>
#include "klientpan.h"




namespace Ui {
class usersign;
}

class usersign : public QDialog
{
    Q_OBJECT

public:
    explicit usersign(QWidget *parent = 0);
    ~usersign();



signals:
    void signalId(int);

private slots:
    void on_register_2_clicked();
    void on_login_btn_clicked();
    void on_pushButton_clicked();

private:
    Ui::usersign *ui;

};

#endif // USERSIGN_H
