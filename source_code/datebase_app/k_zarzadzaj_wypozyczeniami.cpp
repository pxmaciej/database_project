#include "k_zarzadzaj_wypozyczeniami.h"
#include "ui_k_zarzadzaj_wypozyczeniami.h"
#include <QSqlQueryModel>

int kid2;
QString tmp;
k_zarzadzaj_wypozyczeniami::k_zarzadzaj_wypozyczeniami(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::k_zarzadzaj_wypozyczeniami)
{
    ui->setupUi(this);


    tableModel= new QSqlRelationalTableModel(this);
    delegate = new QSqlRelationalDelegate(this);
    tableModel->setTable("wypozyczenia");
    tableModel->setRelation(2, QSqlRelation("klient","id_klienta","nazwa_uzytkownika"));
    tableModel->setRelation(3, QSqlRelation("ksiazka","id_ksiazki","tytul"));

    tableModel->select();


    tableModel->setFilter("klient= "+tmp);
    ui->tableView->setModel(tableModel);

   ui->tableView->setItemDelegate(delegate);
   ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void k_zarzadzaj_wypozyczeniami::klientid(int klient)
{
    kid2 = klient;
    tmp = QString::number(kid2);
}

k_zarzadzaj_wypozyczeniami::~k_zarzadzaj_wypozyczeniami()
{
    delete ui;
}

void k_zarzadzaj_wypozyczeniami::on_pushButton_clicked()
{
    this->close();
    QWidget *parent = this->parentWidget();
    parent->show();
}
