#include "zasoby_biblioteki.h"
#include "ui_zasoby_biblioteki.h"

zasoby_biblioteki::zasoby_biblioteki(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::zasoby_biblioteki)
{
    ui->setupUi(this);
    querymodel=new QSqlQueryModel();
    querymodel->setQuery("SELECT autor.imie AS imię, autor.nazwisko, ksiazka.tytul AS tytuł, ksiazka.podtytul AS podtytuł, ksiazka.id_ksiazki FROM autor INNER JOIN autor_ksiazka ON autor_ksiazka.id_autora=autor.id_autora INNER JOIN ksiazka ON autor_ksiazka.id_ksiazki=ksiazka.id_ksiazki");

    ui->tableView->setModel(querymodel);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

zasoby_biblioteki::~zasoby_biblioteki()
{
    delete ui;
}

