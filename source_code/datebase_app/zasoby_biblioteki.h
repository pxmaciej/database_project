#ifndef ZASOBY_BIBLIOTEKI_H
#define ZASOBY_BIBLIOTEKI_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class zasoby_biblioteki;
}

class zasoby_biblioteki : public QDialog
{
    Q_OBJECT

public:
    explicit zasoby_biblioteki(QWidget *parent = 0);
    ~zasoby_biblioteki();

private slots:

private:
    Ui::zasoby_biblioteki *ui;
    QSqlQueryModel *querymodel;
};

#endif // ZASOBY_BIBLIOTEKI_H
