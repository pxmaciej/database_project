#include "zarzadzaj_uzytkownikami.h"
#include "ui_zarzadzaj_uzytkownikami.h"

zarzadzaj_uzytkownikami::zarzadzaj_uzytkownikami(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::zarzadzaj_uzytkownikami)
{
    ui->setupUi(this);

    tableModel=new QSqlTableModel();
    tableModel->setTable("klient");
    tableModel->select();

     ui->tableView->setModel(tableModel);
     ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

zarzadzaj_uzytkownikami::~zarzadzaj_uzytkownikami()
{
    delete ui;
}
