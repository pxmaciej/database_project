#include "usersign.h"
#include "ui_usersign.h"
#include <QSqlDatabase>
#include <QtSql>
#include <QDebug>

usersign::usersign(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::usersign)
{
    ui->setupUi(this);
}

usersign::~usersign()
{
    delete ui;
}

/************************Rejetracja nowego uzytkownika************************************/
void usersign::on_register_2_clicked()
{
    QString nazwa_uzytkownika=ui->nazwa_uzytkownika->text();
    QString haslo=ui->haslo->text();
    QString PESEL=ui->PESEL->text();
    QString miasto=ui->miasto->text();
    QString telefon=ui->telefon->text();
    QString email=ui->email->text();

    QSqlQuery qry;

    qry.prepare("INSERT INTO klient(nazwa_uzytkownika,haslo,PESEL,miasto,telefon,email)"
                "VALUES (:nazwa_uzytkownika, :haslo, :PESEL, :miasto, :telefon, :email)");

    qry.bindValue(":nazwa_uzytkownika", nazwa_uzytkownika);
    qry.bindValue(":haslo", haslo);
    qry.bindValue(":PESEL", PESEL);
    qry.bindValue(":miasto", miasto);
    qry.bindValue(":telefon", telefon);
    qry.bindValue(":email", email);


    if(qry.exec()){
        QMessageBox::information(this, "Rejestracja", "Rejestracja powiodła się");
    }else{
        QMessageBox::warning(this, "Rejestracja", "Rejestracje nie powiodła się - podana nazwa użytkownika jest zajęta");
    }

    //Czyszczenie wszystkich QlineEdit w formularzu
    foreach(QLineEdit* le, findChildren<QLineEdit*>()) {
    le->clear();
    }


}
/*******************************************************************************************************/


/**********************************Logowanie zarejestrowanego uzytkownika*****************************/
void usersign::on_login_btn_clicked()
{
    QString username, password;
    klientpan *klientpanel = new klientpan;
    username=ui->userlogin->text();
    password=ui->userpassword->text();

    QSqlQuery query;


    if(query.exec("select * from klient where nazwa_uzytkownika='"+username+"' and haslo='"+password+"'")){
        int count=0;

        while(query.next()){
            count ++;
            if(count==1){

                 k_znajdz_ksiazke *kz = new k_znajdz_ksiazke;
                 k_zarzadzaj_wypozyczeniami *kw = new k_zarzadzaj_wypozyczeniami ;
                 connect(this, &usersign::signalId, kz, &k_znajdz_ksiazke::klientid);
                 connect(this, &usersign::signalId, kw, &k_zarzadzaj_wypozyczeniami::klientid);
                 int  klient = query.value(0).toInt();
                 emit signalId(klient);
                 QMessageBox::information(this, "Logowanie","Logowanie się powiodło" );


                 klientpanel=new klientpan(this);
                 klientpanel->show();
                 hide();
            }
            if(count <1){
                 QMessageBox::warning(this, "Logowanie", "Logowanie się nie powiodło");
            }
        }


    }

    //Czyszczenie wszystkich QlineEdit w formularzu
    foreach(QLineEdit* le, findChildren<QLineEdit*>()) {
    le->clear();
    }
}

void usersign::on_pushButton_clicked()
{
    this->hide();

    QWidget *parent = this->parentWidget();
    parent->show();

}
/**************************************************************************************************/


