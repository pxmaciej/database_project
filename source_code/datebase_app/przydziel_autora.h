#ifndef PRZYDZIEL_AUTORA_H
#define PRZYDZIEL_AUTORA_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class przydziel_autora;
}

class przydziel_autora : public QDialog
{
    Q_OBJECT

public:
    explicit przydziel_autora(QWidget *parent = 0);
    ~przydziel_autora();

private slots:
    void on_pushButton_3_clicked();

    void on_pobierz_imie_clicked();

    void on_pobierz_nazwisko_clicked();

    void on_pobierz_tytul_clicked();

private:
    Ui::przydziel_autora *ui;
};

#endif // PRZYDZIEL_AUTORA_H
