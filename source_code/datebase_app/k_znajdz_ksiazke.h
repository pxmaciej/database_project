#ifndef K_ZNAJDZ_KSIAZKE_H
#define K_ZNAJDZ_KSIAZKE_H

#include <QDialog>
#include <QMessageBox>
#include <QTimer>
#include <QSqlDatabase>
#include <QtSql>
#include "klientpan.h"

namespace Ui {
class k_znajdz_ksiazke;
}

class k_znajdz_ksiazke : public QDialog
{
    Q_OBJECT

public:
    explicit k_znajdz_ksiazke(QWidget *parent = nullptr);
    ~k_znajdz_ksiazke();

    void klientid(int);


private slots:
    void on_tableView_activated(const QModelIndex &index);
    void on_szukaj_clicked();
    void on_wypozycz_clicked();



    void on_pushButton_clicked();

    void on_szukaj_2_clicked();

private:
     Ui::k_znajdz_ksiazke *ui;
     QSqlQueryModel* model=new QSqlQueryModel;

     QDateTime curent_time=QDateTime::currentDateTime();
     int idksiazki;
     int naklad;
     QSqlQueryModel *querymodel;
};

#endif // K_ZNAJDZ_KSIAZKE_H
