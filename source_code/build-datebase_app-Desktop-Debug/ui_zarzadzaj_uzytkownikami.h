/********************************************************************************
** Form generated from reading UI file 'zarzadzaj_uzytkownikami.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZARZADZAJ_UZYTKOWNIKAMI_H
#define UI_ZARZADZAJ_UZYTKOWNIKAMI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableView>

QT_BEGIN_NAMESPACE

class Ui_zarzadzaj_uzytkownikami
{
public:
    QTableView *tableView;

    void setupUi(QDialog *zarzadzaj_uzytkownikami)
    {
        if (zarzadzaj_uzytkownikami->objectName().isEmpty())
            zarzadzaj_uzytkownikami->setObjectName(QStringLiteral("zarzadzaj_uzytkownikami"));
        zarzadzaj_uzytkownikami->resize(798, 465);
        tableView = new QTableView(zarzadzaj_uzytkownikami);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setGeometry(QRect(-5, 1, 811, 471));
        tableView->setStyleSheet(QLatin1String("#tableView{\n"
"border-image: url(:/items/manage.png);\n"
"	color: rgb(255, 255, 255);\n"
"}"));

        retranslateUi(zarzadzaj_uzytkownikami);

        QMetaObject::connectSlotsByName(zarzadzaj_uzytkownikami);
    } // setupUi

    void retranslateUi(QDialog *zarzadzaj_uzytkownikami)
    {
        zarzadzaj_uzytkownikami->setWindowTitle(QApplication::translate("zarzadzaj_uzytkownikami", "Zarz\304\205dzaj u\305\274ytkownikami", nullptr));
    } // retranslateUi

};

namespace Ui {
    class zarzadzaj_uzytkownikami: public Ui_zarzadzaj_uzytkownikami {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZARZADZAJ_UZYTKOWNIKAMI_H
