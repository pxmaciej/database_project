/********************************************************************************
** Form generated from reading UI file 'usersign.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERSIGN_H
#define UI_USERSIGN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_usersign
{
public:
    QGroupBox *groupBox;
    QLabel *label_2;
    QLineEdit *nazwa_uzytkownika;
    QLabel *label_3;
    QLineEdit *haslo;
    QPushButton *register_2;
    QLabel *label_4;
    QLineEdit *PESEL;
    QLabel *label_7;
    QLineEdit *miasto;
    QLabel *label_8;
    QLineEdit *telefon;
    QLabel *label_9;
    QLineEdit *email;
    QGroupBox *groupBox_3;
    QLabel *label_5;
    QLineEdit *userlogin;
    QLabel *label_6;
    QLineEdit *userpassword;
    QPushButton *login_btn;
    QPushButton *pushButton;

    void setupUi(QDialog *usersign)
    {
        if (usersign->objectName().isEmpty())
            usersign->setObjectName(QStringLiteral("usersign"));
        usersign->resize(648, 481);
        usersign->setStyleSheet(QLatin1String("#usersign{\n"
"border-image: url(:/items/login_background.jpg);}"));
        groupBox = new QGroupBox(usersign);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 30, 181, 401));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(12);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        groupBox->setFont(font);
        groupBox->setStyleSheet(QLatin1String("QGroupBox{\n"
"background-color: rgb(233, 185, 110);\n"
"border-radius: 10px;\n"
"\n"
"}"));
        groupBox->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 50, 131, 17));
        nazwa_uzytkownika = new QLineEdit(groupBox);
        nazwa_uzytkownika->setObjectName(QStringLiteral("nazwa_uzytkownika"));
        nazwa_uzytkownika->setGeometry(QRect(10, 70, 151, 25));
        nazwa_uzytkownika->setClearButtonEnabled(true);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 100, 64, 17));
        haslo = new QLineEdit(groupBox);
        haslo->setObjectName(QStringLiteral("haslo"));
        haslo->setGeometry(QRect(10, 120, 151, 25));
        haslo->setEchoMode(QLineEdit::Password);
        haslo->setClearButtonEnabled(true);
        register_2 = new QPushButton(groupBox);
        register_2->setObjectName(QStringLiteral("register_2"));
        register_2->setGeometry(QRect(50, 360, 83, 25));
        register_2->setStyleSheet(QStringLiteral("background-color: rgb(239, 41, 41);"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 150, 64, 17));
        PESEL = new QLineEdit(groupBox);
        PESEL->setObjectName(QStringLiteral("PESEL"));
        PESEL->setGeometry(QRect(10, 170, 151, 25));
        PESEL->setEchoMode(QLineEdit::Password);
        PESEL->setClearButtonEnabled(true);
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 200, 64, 17));
        miasto = new QLineEdit(groupBox);
        miasto->setObjectName(QStringLiteral("miasto"));
        miasto->setGeometry(QRect(10, 220, 151, 25));
        miasto->setClearButtonEnabled(true);
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 250, 101, 17));
        telefon = new QLineEdit(groupBox);
        telefon->setObjectName(QStringLiteral("telefon"));
        telefon->setGeometry(QRect(10, 270, 151, 25));
        telefon->setClearButtonEnabled(true);
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 300, 101, 17));
        email = new QLineEdit(groupBox);
        email->setObjectName(QStringLiteral("email"));
        email->setGeometry(QRect(10, 320, 151, 25));
        email->setClearButtonEnabled(true);
        groupBox_3 = new QGroupBox(usersign);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(410, 110, 181, 221));
        groupBox_3->setFont(font);
        groupBox_3->setStyleSheet(QLatin1String("QGroupBox{\n"
"background-color: rgb(138, 226, 52);\n"
"border-radius: 10px;\n"
"\n"
"}"));
        groupBox_3->setAlignment(Qt::AlignCenter);
        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 50, 131, 17));
        userlogin = new QLineEdit(groupBox_3);
        userlogin->setObjectName(QStringLiteral("userlogin"));
        userlogin->setGeometry(QRect(10, 70, 151, 25));
        userlogin->setClearButtonEnabled(true);
        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 100, 64, 17));
        userpassword = new QLineEdit(groupBox_3);
        userpassword->setObjectName(QStringLiteral("userpassword"));
        userpassword->setGeometry(QRect(10, 120, 151, 25));
        userpassword->setEchoMode(QLineEdit::Password);
        userpassword->setClearButtonEnabled(true);
        login_btn = new QPushButton(groupBox_3);
        login_btn->setObjectName(QStringLiteral("login_btn"));
        login_btn->setGeometry(QRect(50, 170, 83, 25));
        login_btn->setStyleSheet(QStringLiteral("background-color: rgb(114, 159, 207);"));
        pushButton = new QPushButton(usersign);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(520, 420, 81, 22));
        pushButton->setStyleSheet(QStringLiteral("background-color: rgb(170, 0, 255);"));

        retranslateUi(usersign);
        QObject::connect(pushButton, SIGNAL(clicked()), groupBox_3, SLOT(hide()));

        QMetaObject::connectSlotsByName(usersign);
    } // setupUi

    void retranslateUi(QDialog *usersign)
    {
        usersign->setWindowTitle(QApplication::translate("usersign", "Rejestracja i logowanie", nullptr));
        groupBox->setTitle(QApplication::translate("usersign", "Za\305\202\303\263\305\274 konto", nullptr));
        label_2->setText(QApplication::translate("usersign", "Nazwa u\305\274ytkownika", nullptr));
        label_3->setText(QApplication::translate("usersign", "Has\305\202o", nullptr));
        haslo->setText(QString());
        register_2->setText(QApplication::translate("usersign", "Rejestracja", nullptr));
        label_4->setText(QApplication::translate("usersign", "PESEL", nullptr));
        PESEL->setText(QString());
        label_7->setText(QApplication::translate("usersign", "Miasto", nullptr));
        miasto->setText(QString());
        label_8->setText(QApplication::translate("usersign", "Numer telefonu", nullptr));
        telefon->setText(QString());
        label_9->setText(QApplication::translate("usersign", "E-mail", nullptr));
        email->setText(QString());
        groupBox_3->setTitle(QApplication::translate("usersign", "Zaloguj si\304\231", nullptr));
        label_5->setText(QApplication::translate("usersign", "Nazwa u\305\274ytkownika", nullptr));
        label_6->setText(QApplication::translate("usersign", "Has\305\202o", nullptr));
        login_btn->setText(QApplication::translate("usersign", "Logowanie", nullptr));
        pushButton->setText(QApplication::translate("usersign", "Wstecz", nullptr));
    } // retranslateUi

};

namespace Ui {
    class usersign: public Ui_usersign {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERSIGN_H
