/********************************************************************************
** Form generated from reading UI file 'dodaj_ksiazke.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DODAJ_KSIAZKE_H
#define UI_DODAJ_KSIAZKE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dodaj_ksiazke
{
public:
    QHBoxLayout *horizontalLayout;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *tytul;
    QLabel *label_2;
    QLineEdit *podtytul;
    QLabel *label_3;
    QComboBox *gatunek;
    QLabel *label_4;
    QComboBox *oprawa;
    QLabel *label_7;
    QLineEdit *ISBN;
    QLabel *label_6;
    QLineEdit *naklad;
    QPushButton *pushButton;

    void setupUi(QDialog *dodaj_ksiazke)
    {
        if (dodaj_ksiazke->objectName().isEmpty())
            dodaj_ksiazke->setObjectName(QStringLiteral("dodaj_ksiazke"));
        dodaj_ksiazke->resize(296, 456);
        dodaj_ksiazke->setStyleSheet(QLatin1String("#dodaj_ksiazke{\n"
"background-color: rgb(255, 211, 35);}"));
        horizontalLayout = new QHBoxLayout(dodaj_ksiazke);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        widget = new QWidget(dodaj_ksiazke);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setStyleSheet(QLatin1String("QGroupBox{\n"
"background-color: rgb(233, 185, 110);\n"
"border-radius: 10px;\n"
"}"));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        tytul = new QLineEdit(widget);
        tytul->setObjectName(QStringLiteral("tytul"));
        tytul->setClearButtonEnabled(true);

        verticalLayout->addWidget(tytul);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        podtytul = new QLineEdit(widget);
        podtytul->setObjectName(QStringLiteral("podtytul"));
        podtytul->setClearButtonEnabled(true);

        verticalLayout->addWidget(podtytul);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        gatunek = new QComboBox(widget);
        gatunek->addItem(QString());
        gatunek->addItem(QString());
        gatunek->addItem(QString());
        gatunek->addItem(QString());
        gatunek->setObjectName(QStringLiteral("gatunek"));

        verticalLayout->addWidget(gatunek);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout->addWidget(label_4);

        oprawa = new QComboBox(widget);
        oprawa->addItem(QString());
        oprawa->addItem(QString());
        oprawa->addItem(QString());
        oprawa->addItem(QString());
        oprawa->setObjectName(QStringLiteral("oprawa"));

        verticalLayout->addWidget(oprawa);

        label_7 = new QLabel(widget);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout->addWidget(label_7);

        ISBN = new QLineEdit(widget);
        ISBN->setObjectName(QStringLiteral("ISBN"));
        ISBN->setClearButtonEnabled(true);

        verticalLayout->addWidget(ISBN);

        label_6 = new QLabel(widget);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout->addWidget(label_6);

        naklad = new QLineEdit(widget);
        naklad->setObjectName(QStringLiteral("naklad"));
        naklad->setClearButtonEnabled(true);

        verticalLayout->addWidget(naklad);

        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setStyleSheet(QLatin1String("#pushButton{\n"
"background-color: rgb(0, 255, 0);}"));

        verticalLayout->addWidget(pushButton);


        horizontalLayout->addWidget(widget);


        retranslateUi(dodaj_ksiazke);

        QMetaObject::connectSlotsByName(dodaj_ksiazke);
    } // setupUi

    void retranslateUi(QDialog *dodaj_ksiazke)
    {
        dodaj_ksiazke->setWindowTitle(QApplication::translate("dodaj_ksiazke", "Dodaj ksi\304\205\305\274k\304\231", nullptr));
        label->setText(QApplication::translate("dodaj_ksiazke", "Tytu\305\202", nullptr));
        label_2->setText(QApplication::translate("dodaj_ksiazke", "Podtytu\305\202", nullptr));
        label_3->setText(QApplication::translate("dodaj_ksiazke", "Gatunek", nullptr));
        gatunek->setItemText(0, QApplication::translate("dodaj_ksiazke", "epika", nullptr));
        gatunek->setItemText(1, QApplication::translate("dodaj_ksiazke", "powiesc", nullptr));
        gatunek->setItemText(2, QApplication::translate("dodaj_ksiazke", "fantastyka", nullptr));
        gatunek->setItemText(3, QApplication::translate("dodaj_ksiazke", "fantasy", nullptr));

        label_4->setText(QApplication::translate("dodaj_ksiazke", "Oprawa", nullptr));
        oprawa->setItemText(0, QApplication::translate("dodaj_ksiazke", "miekka", nullptr));
        oprawa->setItemText(1, QApplication::translate("dodaj_ksiazke", "twarda", nullptr));
        oprawa->setItemText(2, QApplication::translate("dodaj_ksiazke", "zeszytowa", nullptr));
        oprawa->setItemText(3, QApplication::translate("dodaj_ksiazke", "spiralna", nullptr));

        label_7->setText(QApplication::translate("dodaj_ksiazke", "ISBN", nullptr));
        label_6->setText(QApplication::translate("dodaj_ksiazke", "Nak\305\202ad", nullptr));
        pushButton->setText(QApplication::translate("dodaj_ksiazke", "Dodaj", nullptr));
    } // retranslateUi

};

namespace Ui {
    class dodaj_ksiazke: public Ui_dodaj_ksiazke {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DODAJ_KSIAZKE_H
