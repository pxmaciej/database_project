/********************************************************************************
** Form generated from reading UI file 'adminsign.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINSIGN_H
#define UI_ADMINSIGN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_adminsign
{
public:
    QGroupBox *groupBox;
    QLabel *label;
    QLineEdit *adminlogin;
    QLabel *label_2;
    QLineEdit *adminpassword;
    QPushButton *adminlog;
    QPushButton *pushButton;

    void setupUi(QDialog *adminsign)
    {
        if (adminsign->objectName().isEmpty())
            adminsign->setObjectName(QStringLiteral("adminsign"));
        adminsign->resize(648, 481);
        adminsign->setStyleSheet(QLatin1String("#adminsign{\n"
"border-image: url(:/items/login_background.jpg);}"));
        groupBox = new QGroupBox(adminsign);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(250, 100, 141, 231));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        groupBox->setFont(font);
        groupBox->setStyleSheet(QLatin1String("QGroupBox{\n"
"	background-color: rgb(0, 170, 255);\n"
"border-radius: 10px;\n"
"\n"
"}"));
        groupBox->setAlignment(Qt::AlignJustify|Qt::AlignVCenter);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 40, 121, 20));
        adminlogin = new QLineEdit(groupBox);
        adminlogin->setObjectName(QStringLiteral("adminlogin"));
        adminlogin->setGeometry(QRect(10, 70, 113, 22));
        adminlogin->setClearButtonEnabled(true);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 110, 111, 20));
        adminpassword = new QLineEdit(groupBox);
        adminpassword->setObjectName(QStringLiteral("adminpassword"));
        adminpassword->setGeometry(QRect(10, 130, 113, 22));
        adminpassword->setEchoMode(QLineEdit::Password);
        adminpassword->setClearButtonEnabled(true);
        adminlog = new QPushButton(groupBox);
        adminlog->setObjectName(QStringLiteral("adminlog"));
        adminlog->setGeometry(QRect(30, 180, 81, 22));
        adminlog->setStyleSheet(QStringLiteral(""));
        pushButton = new QPushButton(adminsign);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(510, 420, 81, 22));
        pushButton->setStyleSheet(QStringLiteral("background-color: rgb(170, 0, 255);"));

        retranslateUi(adminsign);

        QMetaObject::connectSlotsByName(adminsign);
    } // setupUi

    void retranslateUi(QDialog *adminsign)
    {
        adminsign->setWindowTitle(QApplication::translate("adminsign", "Logowanie administratora", nullptr));
        groupBox->setTitle(QApplication::translate("adminsign", "Zaloguj si\304\231", nullptr));
        label->setText(QApplication::translate("adminsign", "Nazwa u\305\274ytkownika", nullptr));
        label_2->setText(QApplication::translate("adminsign", "Haslo", nullptr));
        adminlog->setText(QApplication::translate("adminsign", "Zaloguj", nullptr));
        pushButton->setText(QApplication::translate("adminsign", "Wstecz", nullptr));
    } // retranslateUi

};

namespace Ui {
    class adminsign: public Ui_adminsign {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINSIGN_H
