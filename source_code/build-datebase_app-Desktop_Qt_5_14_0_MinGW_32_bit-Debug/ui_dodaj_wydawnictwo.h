/********************************************************************************
** Form generated from reading UI file 'dodaj_wydawnictwo.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DODAJ_WYDAWNICTWO_H
#define UI_DODAJ_WYDAWNICTWO_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dodaj_wydawnictwo
{
public:
    QVBoxLayout *verticalLayout_2;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *nazwa;
    QLabel *label_2;
    QDateEdit *data_wydania;
    QLabel *label_3;
    QLineEdit *miejsce_wydania;
    QLabel *label_4;
    QComboBox *typ_publikacji;
    QLabel *label_5;
    QLineEdit *liczba_stron_wydania;
    QLabel *label_6;
    QPushButton *pobierz_liste;
    QComboBox *id_ksiazki;
    QPushButton *Dodaj;

    void setupUi(QDialog *dodaj_wydawnictwo)
    {
        if (dodaj_wydawnictwo->objectName().isEmpty())
            dodaj_wydawnictwo->setObjectName(QString::fromUtf8("dodaj_wydawnictwo"));
        dodaj_wydawnictwo->resize(277, 464);
        dodaj_wydawnictwo->setStyleSheet(QString::fromUtf8("#dodaj_wydawnictwo{\n"
"background-color: rgb(193, 208, 255);}"));
        verticalLayout_2 = new QVBoxLayout(dodaj_wydawnictwo);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        widget = new QWidget(dodaj_wydawnictwo);
        widget->setObjectName(QString::fromUtf8("widget"));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        nazwa = new QLineEdit(widget);
        nazwa->setObjectName(QString::fromUtf8("nazwa"));
        nazwa->setClearButtonEnabled(true);

        verticalLayout->addWidget(nazwa);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        data_wydania = new QDateEdit(widget);
        data_wydania->setObjectName(QString::fromUtf8("data_wydania"));
        data_wydania->setCalendarPopup(true);
        data_wydania->setDate(QDate(2000, 1, 1));

        verticalLayout->addWidget(data_wydania);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout->addWidget(label_3);

        miejsce_wydania = new QLineEdit(widget);
        miejsce_wydania->setObjectName(QString::fromUtf8("miejsce_wydania"));
        miejsce_wydania->setClearButtonEnabled(true);

        verticalLayout->addWidget(miejsce_wydania);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout->addWidget(label_4);

        typ_publikacji = new QComboBox(widget);
        typ_publikacji->addItem(QString());
        typ_publikacji->addItem(QString());
        typ_publikacji->addItem(QString());
        typ_publikacji->addItem(QString());
        typ_publikacji->addItem(QString());
        typ_publikacji->addItem(QString());
        typ_publikacji->setObjectName(QString::fromUtf8("typ_publikacji"));

        verticalLayout->addWidget(typ_publikacji);

        label_5 = new QLabel(widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout->addWidget(label_5);

        liczba_stron_wydania = new QLineEdit(widget);
        liczba_stron_wydania->setObjectName(QString::fromUtf8("liczba_stron_wydania"));
        liczba_stron_wydania->setClearButtonEnabled(true);

        verticalLayout->addWidget(liczba_stron_wydania);

        label_6 = new QLabel(widget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout->addWidget(label_6);

        pobierz_liste = new QPushButton(widget);
        pobierz_liste->setObjectName(QString::fromUtf8("pobierz_liste"));

        verticalLayout->addWidget(pobierz_liste);

        id_ksiazki = new QComboBox(widget);
        id_ksiazki->setObjectName(QString::fromUtf8("id_ksiazki"));

        verticalLayout->addWidget(id_ksiazki);

        Dodaj = new QPushButton(widget);
        Dodaj->setObjectName(QString::fromUtf8("Dodaj"));
        Dodaj->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 255, 25);"));

        verticalLayout->addWidget(Dodaj);


        verticalLayout_2->addWidget(widget);


        retranslateUi(dodaj_wydawnictwo);

        QMetaObject::connectSlotsByName(dodaj_wydawnictwo);
    } // setupUi

    void retranslateUi(QDialog *dodaj_wydawnictwo)
    {
        dodaj_wydawnictwo->setWindowTitle(QCoreApplication::translate("dodaj_wydawnictwo", "Dodaj wydawnictwo", nullptr));
        label->setText(QCoreApplication::translate("dodaj_wydawnictwo", "Nazwa", nullptr));
        label_2->setText(QCoreApplication::translate("dodaj_wydawnictwo", "Data wydania", nullptr));
        data_wydania->setDisplayFormat(QCoreApplication::translate("dodaj_wydawnictwo", "yy/M/d", nullptr));
        label_3->setText(QCoreApplication::translate("dodaj_wydawnictwo", "Miejsce wydania", nullptr));
        label_4->setText(QCoreApplication::translate("dodaj_wydawnictwo", "Typ publikacji", nullptr));
        typ_publikacji->setItemText(0, QCoreApplication::translate("dodaj_wydawnictwo", "ksi\304\205\305\274ka", nullptr));
        typ_publikacji->setItemText(1, QCoreApplication::translate("dodaj_wydawnictwo", "biuletyn", nullptr));
        typ_publikacji->setItemText(2, QCoreApplication::translate("dodaj_wydawnictwo", "broszura", nullptr));
        typ_publikacji->setItemText(3, QCoreApplication::translate("dodaj_wydawnictwo", "ulotka", nullptr));
        typ_publikacji->setItemText(4, QCoreApplication::translate("dodaj_wydawnictwo", "gazeta", nullptr));
        typ_publikacji->setItemText(5, QCoreApplication::translate("dodaj_wydawnictwo", "czasopismo", nullptr));

        label_5->setText(QCoreApplication::translate("dodaj_wydawnictwo", "Liczba stron wydania", nullptr));
        label_6->setText(QCoreApplication::translate("dodaj_wydawnictwo", "ID ksi\304\205\305\274ki", nullptr));
        pobierz_liste->setText(QCoreApplication::translate("dodaj_wydawnictwo", "Pobierz list\304\231", nullptr));
        Dodaj->setText(QCoreApplication::translate("dodaj_wydawnictwo", "Dodaj", nullptr));
    } // retranslateUi

};

namespace Ui {
    class dodaj_wydawnictwo: public Ui_dodaj_wydawnictwo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DODAJ_WYDAWNICTWO_H
