/********************************************************************************
** Form generated from reading UI file 'przydziel_autora.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRZYDZIEL_AUTORA_H
#define UI_PRZYDZIEL_AUTORA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_przydziel_autora
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget_3;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QPushButton *pushButton_3;
    QComboBox *imie_2;
    QComboBox *nazwisko_2;
    QComboBox *tytul_2;
    QPushButton *pobierz_imie;
    QPushButton *pobierz_nazwisko;
    QPushButton *pobierz_tytul;

    void setupUi(QDialog *przydziel_autora)
    {
        if (przydziel_autora->objectName().isEmpty())
            przydziel_autora->setObjectName(QString::fromUtf8("przydziel_autora"));
        przydziel_autora->resize(242, 263);
        przydziel_autora->setStyleSheet(QString::fromUtf8("#przydziel_autora{\n"
"background-color: rgb(255, 170, 255);}"));
        verticalLayout = new QVBoxLayout(przydziel_autora);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget_3 = new QWidget(przydziel_autora);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"background-color: rgb(233, 185, 110);\n"
"border-radius: 10px;\n"
"}"));
        label_14 = new QLabel(widget_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(9, 9, 71, 16));
        label_15 = new QLabel(widget_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(9, 57, 102, 16));
        label_16 = new QLabel(widget_3);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(10, 110, 73, 16));
        pushButton_3 = new QPushButton(widget_3);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(70, 160, 80, 22));
        pushButton_3->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 255, 0);"));
        imie_2 = new QComboBox(widget_3);
        imie_2->setObjectName(QString::fromUtf8("imie_2"));
        imie_2->setGeometry(QRect(9, 29, 181, 22));
        nazwisko_2 = new QComboBox(widget_3);
        nazwisko_2->setObjectName(QString::fromUtf8("nazwisko_2"));
        nazwisko_2->setGeometry(QRect(10, 80, 181, 22));
        tytul_2 = new QComboBox(widget_3);
        tytul_2->setObjectName(QString::fromUtf8("tytul_2"));
        tytul_2->setGeometry(QRect(10, 130, 181, 22));
        pobierz_imie = new QPushButton(widget_3);
        pobierz_imie->setObjectName(QString::fromUtf8("pobierz_imie"));
        pobierz_imie->setGeometry(QRect(200, 30, 21, 22));
        pobierz_nazwisko = new QPushButton(widget_3);
        pobierz_nazwisko->setObjectName(QString::fromUtf8("pobierz_nazwisko"));
        pobierz_nazwisko->setGeometry(QRect(200, 80, 21, 22));
        pobierz_tytul = new QPushButton(widget_3);
        pobierz_tytul->setObjectName(QString::fromUtf8("pobierz_tytul"));
        pobierz_tytul->setGeometry(QRect(200, 130, 21, 22));
        label_14->raise();
        label_15->raise();
        label_16->raise();
        pushButton_3->raise();
        imie_2->raise();
        nazwisko_2->raise();
        tytul_2->raise();
        pobierz_imie->raise();
        pobierz_imie->raise();
        pobierz_nazwisko->raise();
        pobierz_tytul->raise();

        verticalLayout->addWidget(widget_3);


        retranslateUi(przydziel_autora);

        QMetaObject::connectSlotsByName(przydziel_autora);
    } // setupUi

    void retranslateUi(QDialog *przydziel_autora)
    {
        przydziel_autora->setWindowTitle(QCoreApplication::translate("przydziel_autora", "Przydziel autora ksi\304\205zce", nullptr));
        label_14->setText(QCoreApplication::translate("przydziel_autora", "Imi\304\231 autora", nullptr));
        label_15->setText(QCoreApplication::translate("przydziel_autora", "Nazwisko autora", nullptr));
        label_16->setText(QCoreApplication::translate("przydziel_autora", "Tytu\305\202 ksi\304\205\305\274ki", nullptr));
        pushButton_3->setText(QCoreApplication::translate("przydziel_autora", "Dodaj", nullptr));
        pobierz_imie->setText(QCoreApplication::translate("przydziel_autora", "+", nullptr));
        pobierz_nazwisko->setText(QCoreApplication::translate("przydziel_autora", "+", nullptr));
        pobierz_tytul->setText(QCoreApplication::translate("przydziel_autora", "+", nullptr));
    } // retranslateUi

};

namespace Ui {
    class przydziel_autora: public Ui_przydziel_autora {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRZYDZIEL_AUTORA_H
