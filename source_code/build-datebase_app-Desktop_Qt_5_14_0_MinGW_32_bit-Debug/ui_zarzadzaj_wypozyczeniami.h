/********************************************************************************
** Form generated from reading UI file 'zarzadzaj_wypozyczeniami.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZARZADZAJ_WYPOZYCZENIAMI_H
#define UI_ZARZADZAJ_WYPOZYCZENIAMI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_zarzadzaj_wypozyczeniami
{
public:
    QVBoxLayout *verticalLayout;
    QTableView *tableView;

    void setupUi(QDialog *zarzadzaj_wypozyczeniami)
    {
        if (zarzadzaj_wypozyczeniami->objectName().isEmpty())
            zarzadzaj_wypozyczeniami->setObjectName(QString::fromUtf8("zarzadzaj_wypozyczeniami"));
        zarzadzaj_wypozyczeniami->resize(738, 416);
        verticalLayout = new QVBoxLayout(zarzadzaj_wypozyczeniami);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tableView = new QTableView(zarzadzaj_wypozyczeniami);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setStyleSheet(QString::fromUtf8("#tableView{\n"
"border-image: url(:/items/manage.png);\n"
"	color: rgb(255, 255, 255);\n"
"}"));

        verticalLayout->addWidget(tableView);


        retranslateUi(zarzadzaj_wypozyczeniami);

        QMetaObject::connectSlotsByName(zarzadzaj_wypozyczeniami);
    } // setupUi

    void retranslateUi(QDialog *zarzadzaj_wypozyczeniami)
    {
        zarzadzaj_wypozyczeniami->setWindowTitle(QCoreApplication::translate("zarzadzaj_wypozyczeniami", "Zarz\304\205dzaj wypo\305\274yczeniami", nullptr));
    } // retranslateUi

};

namespace Ui {
    class zarzadzaj_wypozyczeniami: public Ui_zarzadzaj_wypozyczeniami {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZARZADZAJ_WYPOZYCZENIAMI_H
